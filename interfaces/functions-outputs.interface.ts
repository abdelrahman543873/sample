export interface Balance {
  id: number;
  coinId: number;
  balance: number;
  userId: number;
}

export interface RestrictedBalance {
  id: number;
  balance: number;
  unlockingWagerAmount: number;
  coinId: number;
  userId: number;
  claimedAt: Date;
  expiredAt: Date;
  awardedAt: Date;
}

export interface getWageringProgressForRestrictedBalance {
  progressPercentage: number;
}
