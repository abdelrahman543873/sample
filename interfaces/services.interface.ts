import {
  AddRestrictedBalanceInput,
  AddXpInput,
  BetInput,
  DeductStreakXpInput,
  DepositInput,
  GetBalanceInput,
  GetRestrictedBalanceInput,
  WithdrawInput,
} from "./inputs.interfaces";
import {
  Balance,
  getWageringProgressForRestrictedBalance,
  RestrictedBalance,
} from "./functions-outputs.interface";
export interface BalanceService {
  deposit(depositInput: DepositInput): Balance;
  withdraw(withdrawInput: WithdrawInput): Balance;
  discardRestrictedBalance(balanceId: number): RestrictedBalance;
  unlockBalanceRestriction(balanceId: number): RestrictedBalance;
  getRestrictedBalances(userId: number): Array<RestrictedBalance>;
  getRestrictedBalance(
    getRestrictedBalanceInput: GetRestrictedBalanceInput
  ): RestrictedBalance;
  getBalances(userId: number): Array<Balance>;
  getBalance(getBalanceInput: GetBalanceInput): Balance;
}

export interface BetService {
  bet(betInput: BetInput);
}
export interface RewardService {
  addRestrictedBalance(
    addRestrictedBalanceInput: AddRestrictedBalanceInput
  ): RestrictedBalance;
  deductStreakXp(deductStreakXpInput: DeductStreakXpInput): number;
  getWageringProgressForRestrictedBalance(
    restrictedBalanceId: number
  ): getWageringProgressForRestrictedBalance;
}

export interface UserService {
  addXp(addXpInput: AddXpInput);
  getXp(userId: number, streak: boolean): number;
  getLevel(userId: number): number;
}
