export interface DepositInput {
  quantity: number;
  coinId: number;
  userId: number;
}
export interface WithdrawInput {
  quantity: number;
  coin_id: number;
}

export interface GetRestrictedBalanceInput {
  coinId: number;
  userId: number;
}

export interface GetBalanceInput {
  userId: number;
  balanceId: number;
}

export interface BetInput {
  quantity: number;
  coinId: number;
  userId: number;
}

export interface AddRestrictedBalanceInput {
  quantity: number;
  userId: number;
  coinId: number;
}

export interface DeductStreakXpInput {
  userId: number;
  quantity: number;
}

export interface AddXpInput {
  quantity: number;
  userId: number;
  streak: boolean;
}
