function getLevel(xp) {
  let previous = 83;
  let beforePrevious = 0;
  let calculation = 0;
  let level = 3;
  if (xp < 83) {
    return 1;
  }
  if (xp < 174) {
    return 2;
  }
  function recursion() {
    calculation = Math.floor(2.1 * previous - 1.1 * beforePrevious);
    if (calculation <= xp) {
      return level + 1;
    }
    level += 1;
    beforePrevious = previous;
    previous = calculation;
    return recursion();
  }
  return recursion();
}

getLevel(638);
