import { UserService } from "../interfaces/services.interface";
describe("get level case", () => {
  it("should get level successfully", async () => {
    let userService;
    let level = (userService as UserService).getLevel(1);
    expect(level).toBe(0);
    (userService as UserService).addXp({
      userId: 1,
      streak: false,
      quantity: 85,
    });
    level = (userService as UserService).getLevel(1);
    expect(level).toBe(1);
  });
});
1;
