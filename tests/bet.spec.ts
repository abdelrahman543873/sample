import { UserService } from "../interfaces/services.interface";
import { Balance } from "../interfaces/functions-outputs.interface";
import {
  BalanceService,
  BetService,
  RewardService,
} from "../interfaces/services.interface";
describe("bet suite case", () => {
  it("should bet successfully", async () => {
    let balanceService;
    let balance = (balanceService as BalanceService).getBalance({
      userId: 1,
      balanceId: 1,
    });
    expect(balance).toBe(0);
    (balanceService as BalanceService).deposit({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    balance = (balanceService as BalanceService).getBalance({
      userId: 1,
      balanceId: 1,
    });
    expect(balance.balance).toBe(1);
    let bet = (balanceService as BetService).bet({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    expect(bet.balance).toBe(1);
    balance = (balanceService as BalanceService).getBalance({
      userId: 1,
      balanceId: 1,
    });
    expect(bet.balance).toBe(0);
  });

  it("should increase xp when wagering successfully", async () => {
    let balanceService;
    let userService;
    let userXp;
    userXp = (userService as UserService).getXp(1, false);
    expect(userXp).toBe(0);
    (balanceService as BalanceService).deposit({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    let bet = (balanceService as BetService).bet({
      userId: 1,
      coinId: 1,
      quantity: 0.5,
    });
    userXp = (userService as UserService).getXp(1, false);
    expect(userXp).toBe(1);
    expect(bet.balance).toBe(0);
  });

  it("should increase streak xp when wagering successfully", async () => {
    let balanceService;
    let userService;
    let userXp;
    userXp = (userService as UserService).getXp(1, true);
    expect(userXp).toBe(0);
    (balanceService as BalanceService).deposit({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    let bet = (balanceService as BetService).bet({
      userId: 1,
      coinId: 1,
      quantity: 0.01,
    });
    userXp = (userService as UserService).getXp(1, true);
    expect(userXp).toBe(1);
    expect(bet.balance).toBe(0);
  });

  it("should subtract from unrestricted balance first before subtracting from restricted balance when betting", async () => {
    let balanceService;
    (balanceService as BalanceService).deposit({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    let rewardService;
    expect(balanceService.getBalance(1, 1)).toBe(0);
    (rewardService as RewardService).addRestrictedBalance({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    let bet = (balanceService as BetService).bet({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    expect(bet.balance).toBe(1);
    let restrictedBalance = (
      balanceService as BalanceService
    ).getRestrictedBalance({
      userId: 1,
      coinId: 1,
    });
    expect(restrictedBalance.balance).toBe(0);
    let balance = (balanceService as BalanceService).getBalance({
      userId: 1,
      balanceId: 1,
    });
    expect(balance.balance).toBe(0);
  });

  it("should distribute winnings accordingly", async () => {
    let balanceService;
    let rewardService;
    (balanceService as BalanceService).deposit({
      userId: 1,
      coinId: 1,
      quantity: 100,
    });
    (rewardService as RewardService).addRestrictedBalance({
      userId: 1,
      coinId: 1,
      quantity: 50,
    });
    (balanceService as BetService).bet({
      userId: 1,
      coinId: 1,
      quantity: 120,
    });
    //assuming the bet is won
    let balanceAfterWinning = (balanceService as BalanceService).getBalance({
      userId: 1,
      balanceId: 1,
    });
    expect(balanceAfterWinning.balance).toBe(166.4);
    let restrictedBalanceAfterWinning = (
      balanceService as BalanceService
    ).getRestrictedBalance({
      userId: 1,
      coinId: 1,
    });
    expect(restrictedBalanceAfterWinning.balance).toBe(63.6);
  });
});
