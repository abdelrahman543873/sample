import {
  BalanceService,
  BetService,
  RewardService,
} from "../interfaces/services.interface";
describe("withdraw suite case", () => {
  it("should withdraw successfully", async () => {
    let balanceService;
    let balance = (balanceService as BalanceService).getBalance({
      userId: 1,
      balanceId: 1,
    });
    expect(balance).toBe(0);
    (balanceService as BalanceService).deposit({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    balance = (balanceService as BalanceService).getBalance({
      userId: 1,
      balanceId: 1,
    });
    expect(balance.balance).toBe(1);
    balance = (balanceService as BalanceService).withdraw({
      quantity: 1,
      coin_id: 1,
    });
    expect(balance.balance).toBe(1);
  });

  it("shouldn't withdraw restricted balance", async () => {
    let balanceService;
    let balance = (balanceService as BalanceService).getBalance({
      userId: 1,
      balanceId: 1,
    });
    expect(balance).toBe(0);
    (balanceService as RewardService).addRestrictedBalance({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    balance = (balanceService as BalanceService).getBalance({
      userId: 1,
      balanceId: 1,
    });
    expect(balance.balance).toBe(1);
    expect(
      (balanceService as BalanceService).withdraw({ quantity: 1, coin_id: 1 })
    ).toThrowError();
  });

  it("should withdraw restricted balance when wagering requirement is met", async () => {
    let balanceService;
    let rewardService;
    let betService;
    (rewardService as RewardService).addRestrictedBalance({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    // satisfying the wagering requirement by betting with 100x
    (balanceService as BalanceService).deposit({
      userId: 1,
      coinId: 1,
      quantity: 100,
    });
    (betService as BetService).bet({
      userId: 1,
      coinId: 1,
      quantity: 100,
    });
    // assuming that the deposited money is still in betting stage and i have satisfied the wagering requirement i should only have the restricted balance left and i should be able to withdraw it
    expect(
      (balanceService as BalanceService).withdraw({
        quantity: 1,
        coin_id: 1,
      }).balance
    ).toBe(1);
  });

  it("should calculate wagering requirement correctly", async () => {
    let balanceService;
    let rewardService;
    let betService;
    (rewardService as RewardService).addRestrictedBalance({
      userId: 1,
      coinId: 1,
      quantity: 5,
    });
    (balanceService as BalanceService).deposit({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    (betService as BetService).bet({
      userId: 1,
      coinId: 1,
      quantity: 6,
    });
    expect(
      (rewardService as RewardService).getWageringProgressForRestrictedBalance(
        1
      ).progressPercentage
    ).toBe((6 / 100) * 100);
  });

  it("shouldn't withdraw if unrestricted balance isn't sufficient", async () => {
    let balanceService;
    let balance = (balanceService as BalanceService).getBalance({
      userId: 1,
      balanceId: 1,
    });
    expect(balance).toBe(0);
    (balanceService as BalanceService).deposit({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    balance = (balanceService as BalanceService).getBalance({
      userId: 1,
      balanceId: 1,
    });
    expect(balance.balance).toBe(1);
    expect(
      (balanceService as BalanceService).withdraw({
        coin_id: 2,
        quantity: 1,
      })
    ).toThrowError();
  });
});
