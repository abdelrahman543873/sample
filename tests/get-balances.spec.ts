import { BalanceService } from "../interfaces/services.interface";
describe("get balances suite case", () => {
  it("should get balances", async () => {
    let balanceService;
    const balance = (balanceService as BalanceService).getBalance(1, 1);
    expect(balance).toBe(0);
    (balanceService as BalanceService).deposit({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    expect(
      (balanceService as BalanceService).getBalance({
        userId: 1,
        balanceId: 1,
      }).balance
    ).toBe(1);
    (balanceService as BalanceService).deposit({
      userId: 1,
      coinId: 2,
      quantity: 1,
    });
    expect(
      (balanceService as BalanceService).getBalance({
        userId: 1,
        balanceId: 1,
      }).balance
    ).toBe(1);
    expect((balanceService as BalanceService).getBalances(1).length).toBe(2);
  });
});
9;
