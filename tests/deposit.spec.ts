import { BalanceService } from "../interfaces/services.interface";
describe("deposit suite case", () => {
  it("should deposit successfully", async () => {
    let balanceService;
    let balance = (balanceService as BalanceService).getBalance({
      userId: 1,
      balanceId: 1,
    });
    expect(balance).toBe(0);
    (balanceService as BalanceService).deposit({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    balance = (balanceService as BalanceService).getBalance({
      userId: 1,
      balanceId: 1,
    });
    expect(balance.balance).toBe(1);
  });

  it("should go to unrestricted balance only when depositing", async () => {
    let balanceService;
    let balance = (balanceService as BalanceService).getBalance({
      userId: 1,
      balanceId: 1,
    });
    expect(balance).toBe(0);
    (balanceService as BalanceService).deposit({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    balance = (balanceService as BalanceService).getBalance({
      userId: 1,
      balanceId: 1,
    });
    expect(balance.balance).toBe(1);
    let restrictedBalances = (
      balanceService as BalanceService
    ).getRestrictedBalances(1);
    expect(restrictedBalances.length).toBe(0);
  });
});
