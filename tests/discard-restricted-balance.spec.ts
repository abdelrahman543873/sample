import {
  BalanceService,
  RewardService,
} from "../interfaces/services.interface";
describe("discard restricted balance case", () => {
  it("should discard restricted balance", async () => {
    let balanceService;
    let balance = (balanceService as BalanceService).getRestrictedBalances(1);
    expect(balance.length).toBe(0);
    let rewardService;
    (rewardService as RewardService).addRestrictedBalance({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    balance = (balanceService as BalanceService).getRestrictedBalances(1);
    expect(balance.length).toBe(1);
  });
});
1;
