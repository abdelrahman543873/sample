import {
  BalanceService,
  RewardService,
} from "../interfaces/services.interface";
describe("add restricted balance suite case", () => {
  it("should add restricted balance", async () => {
    let rewardService;
    let balanceService;
    let restrictedBalance = (
      balanceService as BalanceService
    ).getRestrictedBalance({ coinId: 1, userId: 1 });
    expect(restrictedBalance.balance).toBe(0);
    restrictedBalance = (rewardService as RewardService).addRestrictedBalance({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    expect(restrictedBalance.balance).toBe(1);
  });

  it("should be able to have more than one restricted balance", async () => {
    let rewardService;
    let balanceService;
    let restrictedBalance;
    expect(
      (balanceService as BalanceService).getRestrictedBalance({
        userId: 1,
        coinId: 1,
      }).balance
    ).toBe(0);
    (rewardService as RewardService).addRestrictedBalance({
      userId: 1,
      coinId: 1,
      quantity: 1,
    });
    expect(
      (balanceService as BalanceService).getRestrictedBalance(1, 1).balance
    ).toBe(1);
    (rewardService as RewardService).addRestrictedBalance({
      userId: 1,
      quantity: 1,
      coinId: 2,
    });
    expect(
      (balanceService as BalanceService).getRestrictedBalance({
        userId: 1,
        coinId: 2,
      }).balance
    ).toBe(1);
    (balanceService as BalanceService).getBalances(1);
    expect(restrictedBalance.length).toBe(2);
  });
});
1;
